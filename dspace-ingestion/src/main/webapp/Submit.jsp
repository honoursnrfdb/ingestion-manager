<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by: Darryl Meyer
  Date: 15/09/08
  Time: 3:44 PM

  Modified by: Craig Feldman
  Date: 21/09/
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ingestion Manager - Submit</title>
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
</head>
<body role="document" onbeforeunload='setDefaultDropDown();'>

<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${pageContext.request.contextPath}">Ingestion Manager</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="${pageContext.request.contextPath}"><span class="glyphicon glyphicon-home"></span> Home</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <%
                        if (session.getAttribute("user_email") != null) {
                    %>
                    <a><span class="glyphicon glyphicon-user"></span> Logged&nbsp;in&nbsp;as: ${user_email}</a>
                    <%
                    } else {
                    %>
                    <a href="${pageContext.request.contextPath}"><span class=" glyphicon glyphicon-user"></span> Not&nbsp;logged&nbsp;in</a>
                    <%
                        }
                    %>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/logout"><span class="glyphicon glyphicon-log-out"></span>
                        Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <%
        if (session.getAttribute("user_email") != null && (Boolean) session.getAttribute("user")) {
    %>
    <!-- Main Submission Content -->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Upload Batch</h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" id="form-submission-loader"
                  action="${pageContext.request.contextPath}/metadata-mapper"
                  method="post" enctype="multipart/form-data">
                <p class="help-block">To get started, upload the file that contains the data you would like to ingest into a Collection.
                    You can also save the metadata mapping for future use, or use a previously saved mapping.</p>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="file-upload">
                        Input file
                         <span class="glyphicon glyphicon-info-sign" aria-hidden="true" data-toggle="popover"
                               data-container="body"
                               title="File upload" data-content="<p>Browse for the CSV file you wish to upload.</p><p>If a field spans over multiple lines or includes a separator character, enclose it in inverted commas.
                           </p>If a field contains an inverted comma, you need to escape it by preceding it with a backslash."></span>
                        :
                    </label>

                    <div class="col-md-6">
                        <input class="form-control submission-file-loader" name="file-upload" id="file-upload"
                               type="file" accept=".csv" required="required">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="has-header">Has header:</label>

                    <div class="col-md-6">
                        <input type="checkbox" name="has-header" id="has-header"> The first line of the input file
                        contains headings.
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="separators">Field separator:</label>

                    <div class="col-md-2">
                        <select class="form-control" name="separators" id="separators" required="required">
                            <option value="">Select...</option>
                            <option value=","> , [comma]</option>
                            <option value=";"> ; [semi-colon]</option>
                            <option value="\t">\t [tab]</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="save-mapping-as">
                         Save mapping as
                         <span class="glyphicon glyphicon-question-sign" aria-hidden="true"
                               data-toggle="tooltip" data-container="body"
                               title="If you would like to save the metadata mapping for future use, specify the name to save it as."></span>
                        :
                    </label>

                    <div class="col-md-6">
                        <input type="text" name="save-mapping-as" id="save-mapping-as" placeholder="[Mapping will not be saved]"
                               class="form-control" aria-label="...">
                    </div>
                </div>

                <!-- LOAD SAVED MAPPINGS LIST -->
                <div class="form-group">
                    <label class="col-md-3 control-label" for="load-mapping-file">
                        Use previous mapping
                         <span class="glyphicon glyphicon-question-sign" aria-hidden="true"
                               data-toggle="tooltip" data-container="body"
                               title="If you have previously saved a mapping for this input format, you can load it here."></span>
                        :
                    </label>

                    <div class="col-md-6">
                        <select class="form-control" name="load-mapping-file" id="load-mapping-file">
                            <option value="">[I am creating a new mapping]</option>
                            <c:forEach items="${savedMappings}" var="item">
                                <option>${item}</option>
                            </c:forEach>

                        </select>
                    </div>
                </div>


                <div class="form-group" id="select-collection-file-div">
                    <label class="col-md-3 control-label" for="select-collection">Collection:</label>

                    <div class="col-md-6">
                        <select name="select-collection" class="form-control submission-file-loader"
                                id="select-collection" required="required">
                            <option value="">Select...</option>
                            <%
                                if (session.getAttribute("collections") != null) {
                            %>
                            ${collections}
                            <%
                                }
                            %>
                        </select>

                    </div>

                </div>

                <div class="form-group">
                    <div class="col-md-12">

                        <input type="submit" class="btn btn-primary pull-left" name="submit" value="Submit"
                               tabindex="3"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Previous Uploads</h3>
        </div>
        <div class="panel-body">

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#pending" aria-controls="pending" role="tab"
                                                          data-toggle="tab"><b>Submissions Pending Approval</b></a></li>
                <li role="presentation"><a href="#previous" aria-controls="previous" role="tab" data-toggle="tab"><b>My
                    Previously Approved Submissions</b></a></li>
            </ul>

            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="pending">
                    <%
                        if (session.getAttribute("user_pending_submissions") != null) {
                    %>
                    <p class="help-block">These batches are still to be approved.</p>
                    <table class="table" summary="Table listing submissions pending approval" align="center">
                        <tbody>
                        <tr>
                            <th class="oddRowOddCol">&nbsp;</th>
                            <th id="t11" class="oddRowOddCol">Filename</th>
                            <th id="t12" class="oddRowEvenCol">Submitted to</th>
                            <th id="t13" class="oddRowOddCol">&nbsp;</th>
                        </tr>
                        ${user_pending_submissions}
                        </tbody>
                    </table>
                    <%
                    } else {
                    %>
                    <p class="help-block">No pending submissions.</p>
                    <%
                        }
                    %>

                </div>
                <div role="tabpanel" class="tab-pane" id="previous">
                    <%
                        if (session.getAttribute("user_approved_submissions") != null) {
                    %>
                    <p class="help-block">These batches have been approved and entered into the DSpace repository.</p>
                    <table class="table" summary="Table listing approved submissions" align="center">
                        <tbody>
                        <tr>
                            <th class="oddRowOddCol">&nbsp;</th>
                            <th id="t21" class="oddRowOddCol">Filename</th>
                            <th id="t22" class="oddRowEvenCol">Submitted to</th>
                        </tr>

                        ${user_approved_submissions}
                        </tbody>
                    </table>
                    <%
                    } else {
                    %>
                    <p class="help-block">No approved submissions.</p>
                    <%
                        }
                    %>
                </div>
            </div>
        </div>
    </div>
    <%
    } else {
    %>
    <!-- User is not logged in -->
    <p>You are not logged in. Please login to submit.</p>
    <a href="${pageContext.request.contextPath}">
        <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-home"></span> Go home</button>
    </a>
    <%
        }
    %>
</div>

<script src="bootstrap/js/jquery-1.11.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="bootstrap/js/docs.min.js"></script>

<script type="text/javascript" language="javascript">
    function setDefaultDropDown() {
        document.getElementById("select-collection").selectedIndex = -1;
    }

    // executes these commands when page has loaded
    $(document).ready(function () {
        // enable tooltips
        $('[data-toggle="tooltip"]').tooltip({container: "body"});
        // enable popovers
        $('[data-toggle="popover"]').popover({html: true, trigger: "hover"});
    });
</script>

</body>
</html>