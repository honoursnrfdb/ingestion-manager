<%--
  Created by: Darryl Meyer
  Date: 15/09/08
  Time: 2:58 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ingestion Manager - Login</title>
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
</head>
<body role="document">

<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${pageContext.request.contextPath}">Ingestion Manager</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="${pageContext.request.contextPath}"><span class="glyphicon glyphicon-home"></span>
                    Home</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">

                <%
                    if (session.getAttribute("user_email") != null) {
                %>
                <li>
                    <a><span class="glyphicon glyphicon-user"></span> Logged&nbsp;in&nbsp;as: ${user_email}</a>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/logout"><span class="glyphicon glyphicon-log-out"></span>
                        Logout</a>
                </li>
                <%
                } else {
                %>
                <li>
                    <a href="${pageContext.request.contextPath}"><span class="glyphicon glyphicon-user"></span> Not&nbsp;logged&nbsp;in</a>
                </li>
                <%
                    }
                %>
            </ul>
        </div>
    </div>
</nav>
<div class="container">

    <%
        if (session.getAttribute("logout_success_title") != null) {
    %>
    <div class="alert alert-success">
        <strong>${logout_success_title}</strong> ${logout_success_message}
    </div>
    <%
            session.setAttribute("logout_success_title", null);
            session.setAttribute("logout_success_message", null);
        }
    %>

    <%
        if (session.getAttribute("logout_failure_title") != null) {
    %>
    <div class="alert alert-danger">
        <strong>${logout_failure_title}</strong> ${logout_failure_message}
    </div>
    <%
            session.setAttribute("logout_failure_title", null);
            session.setAttribute("logout_failure_message", null);
        }
    %>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Ingestion Manager</h3>
        </div>
        <div class="panel-body">
            This add-on for DSpace allows the users of a DSpace repository to remotely ingest batches of items into the
            repository. Batch submissions enter into a holding area after successful user submission and once approved by a
            manager the batch will be entered into the DSpace repository. Users can remove batches that are still in the
            holding area waiting for manager approval. Managers can approve or remove batches from the holding area.
        </div>
    </div>

    <%
        if (session.getAttribute("user_email") == null) {
    %>
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Login</h3>
        </div>
        <div class="panel-body">
            <form name="loginform" class="form-horizontal" id="loginform" method="post"
                  action="${pageContext.request.contextPath}/login">
                <p>Please enter your e-mail address and password you use to log into DSpace in the form below.</p>

                <div class="form-group">
                    <label class="col-md-offset-3 col-md-2 control-label" for="tlogin_email">E-mail Address:</label>

                    <div class="col-md-3">
                        <input class="form-control" type="text" name="login_email" id="tlogin_email" tabindex="1" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-offset-3 col-md-2 control-label" for="tlogin_password">Password:</label>

                    <div class="col-md-3">
                        <input class="form-control" type="password" name="login_password" id="tlogin_password" tabindex="2"
                               required/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <input type="submit" class="btn btn-success pull-right" name="login_submit" value="Log In" tabindex="3"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<%
} else if ((Boolean) session.getAttribute("user")) {
%>
<a href="${pageContext.request.contextPath}/update">
    <button type="button" class="btn btn-default"> Upload Batch or View Previous Uploads</button>
</a>
<%
} else if ((Boolean) session.getAttribute("manager")) {
%>
<a href="${pageContext.request.contextPath}/update">
    <button type="button" class="btn btn-default"> Review Pending Submissions</button>
</a>
<%
    }
%>

<script src="bootstrap/js/jquery-1.11.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="bootstrap/js/docs.min.js"></script>
</body>
</html>
