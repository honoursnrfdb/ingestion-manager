<%--
  Created by: darrylmeyer
  Date: 15/09/10
  Time: 4:46 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ingestion Manager - Approve</title>
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap theme -->
    <link href="bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">
</head>
<body role="document">

<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${pageContext.request.contextPath}">Ingestion Manager</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li><a href="${pageContext.request.contextPath}"><span class="glyphicon glyphicon-home"></span> Home</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <%
                        if (session.getAttribute("user_email") != null) {
                    %>
                    <a><span class="glyphicon glyphicon-user"></span> Logged&nbsp;in&nbsp;as: ${user_email}</a>
                    <%
                    } else {
                    %>
                    <a href="${pageContext.request.contextPath}"><span class="glyphicon glyphicon-user"></span> Not&nbsp;logged&nbsp;in</a>
                    <%
                        }
                    %>
                </li>
                <li>
                    <a href="${pageContext.request.contextPath}/logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="container">
    <%
        if (session.getAttribute("user_email") != null && (Boolean) session.getAttribute("manager")) {
    %>
    <!-- Main Submission Content -->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Submissions Pending Approval</h3>
        </div>
        <div class="panel-body">
            <%
                if (session.getAttribute("pending_submissions") != null) {
            %>
            <p class="help-block">These batches are pending approval.</br>If you need to remove a submission, consider sending an
                email to the user before removing a pending submission. Click on the user's email address to send them an email
                with regard to why you will not approve the submission.
            </p>

            <table class="table" summary="Table listing submissions pending approval" align="center">
                <tbody>
                <tr>
                    <th class="oddRowOddCol">&nbsp;</th>
                    <th id="t11" class="oddRowEvenCol">&nbsp;</th>
                    <th id="t12" class="oddRowOddCol">User</th>
                    <th id="t13" class="oddRowOddCol">Filename</th>
                    <th id="t14" class="oddRowEvenCol">Submitted to</th>
                    <th id="t15" class="oddRowOddCol">&nbsp;</th>
                </tr>

                ${pending_submissions}
                </tbody>
            </table>
            <%
            } else { %>
            <p class="help-block">There are no batches pending approval.</p>
            <%
                }
            %>

        </div>
    </div>
    <%
    } else {
    %>
    <!-- User is not logged in -->
    <p>You are not logged in. Please login to approve submissions.</p>
    <a href="${pageContext.request.contextPath}">
        <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-home"></span> Go home</button>
    </a>
    <%
        }
    %>
</div>

<script src="bootstrap/js/jquery-1.11.3.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="bootstrap/js/docs.min.js"></script>
</body>
</html>

