package za.ac.uct.ingestion.servlets;

import za.ac.uct.ingestion.data.Submission;
import za.ac.uct.ingestion.storage.DatabaseController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Darryl Meyer
 * 16 September 2015
 * <p>
 * Handles the viewing of the submission details.
 */
@WebServlet(name = "ViewSubmission")
public class ViewSubmission extends HttpServlet {

    private String uploadsPath = "upload" + File.separator;

    /*
     * Coordinates the retrieval of the submission details from the database, sets session attributes and redirects upon
     * success or failure.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");

        // Load the submission data from the database
        String resourcesPath = File.separator + "WEB-INF" + File.separator + "classes"  + File.separator;
        String pathToDatabase = getServletContext().getRealPath(resourcesPath);
        DatabaseController database = new DatabaseController(pathToDatabase);

        String email = request.getSession().getAttribute("user_email").toString();

        String ID = request.getParameter("submission_id");

        Submission submission = database.selectSubmission(ID);

        if (submission != null) {
            String submissionContent = getSubmissionContentTable(submission);
            request.getSession().setAttribute("submission_details", submissionContent);
            String submissionPreview = getSubmissionPreview(submission);
            request.getSession().setAttribute("submission_preview", submissionPreview);

            String fileLocation = submission.getMetadataFileAbsolutePath();
            Path path = Paths.get(fileLocation);
            request.getSession().setAttribute("download_link", uploadsPath + path.getFileName().toString());
            request.getSession().setAttribute("download_filename", path.getFileName().toString());

            if (database.isUserManager(email)) {
                request.getSession().setAttribute("return_manager", "Back to pending approval list");
                request.getSession().setAttribute("return_user", null);
            } else {
                request.getSession().setAttribute("return_manager", null);
                request.getSession().setAttribute("return_user", "Back to submissions");
            }
            response.sendRedirect("/ingest/View.jsp");
        } else {
            request.getSession().setAttribute("title", "View Unsuccessful.");
            request.getSession().setAttribute("message", "The submission you selected could not be viewed at this time.");
            if (database.isUserManager(email)) {
                request.getSession().setAttribute("return_manager", "Back to pending approval list");
                request.getSession().setAttribute("return_user", null);
            } else {
                request.getSession().setAttribute("return_manager", null);
                request.getSession().setAttribute("return_user", "Back to submissions");
            }
            response.sendRedirect("/ingest/Error.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendRedirect("/ingest");
    }

    private String getSubmissionPreview(Submission submission){
        StringBuilder preview = new StringBuilder();
        int lineMax = 10;
        int lineCount = 0;
        try {
            BufferedReader br = new BufferedReader(new FileReader(submission.getMetadataFileAbsolutePath()));
            String line;
            while ((line = br.readLine()) != null && lineCount < lineMax) {
                preview.append(line).append("</br>");
                lineCount++;
            }
        } catch (IOException e) {
            logErr("Could not load file. " + e.getMessage());
        }

        return preview.toString();
    }

    /*
     * Reads the contents of a file to a string.
     * @param pathToFile Absolute path to file to read.
     * @return String of the contents of the file.
     */
    private String readContentFromFile(String pathToFile) {

        byte[] bytes;
        String fileContents = null;

        try {
            bytes = Files.readAllBytes(Paths.get(pathToFile));
            fileContents = new String(bytes, StandardCharsets.UTF_8);
        } catch (IOException e) {
            logErr("Could not read data from file. " + e.getMessage());
        }
        return fileContents;
    }

    /*
     * Builds an HTML table filled with the details of the submission.
     */
    private String getSubmissionContentTable(Submission submission) {

        Path path = Paths.get(submission.getMetadataFileAbsolutePath());
        String filename = path.getFileName().toString();
        StringBuilder tableContent = new StringBuilder();
        tableContent.append("            <tr>\n");
        tableContent.append("                <td>\n");
        tableContent.append("                    User email\n");
        tableContent.append("                </td>\n");
        tableContent.append("                <td>\n");
        tableContent.append("                    " + submission.getUserEmail() + "\n");
        tableContent.append("                </td>\n");
        tableContent.append("            </tr>\n");
        tableContent.append("            <tr>\n");
        tableContent.append("                <td>\n");
        tableContent.append("                    Collection Name\n");
        tableContent.append("                </td>\n");
        tableContent.append("                <td>\n");
        tableContent.append("                    " + submission.getCollectionName() + "\n");
        tableContent.append("                </td>\n");
        tableContent.append("            </tr>");
        tableContent.append("            <tr>\n");
        tableContent.append("                <td>\n");
        tableContent.append("                    Filename\n");
        tableContent.append("                </td>\n");
        tableContent.append("                <td>\n");
        tableContent.append("                    " + filename + "\n");
        tableContent.append("                </td>\n");
        tableContent.append("            </tr>\n");
        tableContent.append("            <tr>\n");
        tableContent.append("                <td>\n");
        tableContent.append("                    Date Added\n");
        tableContent.append("                </td>\n");
        tableContent.append("                <td>\n");
        tableContent.append("                    " + submission.getDateAdded() + "\n");
        tableContent.append("                </td>\n");
        tableContent.append("            </tr>\n");
        tableContent.append("            <tr>\n");
        tableContent.append("                <td>\n");
        tableContent.append("                    Date Approved\n");
        tableContent.append("                </td>\n");
        tableContent.append("                <td>\n");
        tableContent.append("                    " + submission.getDateApproved() + "\n");
        tableContent.append("                </td>\n");
        tableContent.append("            </tr>\n");

        return tableContent.toString();
    }

    private void logln(String str) {

        System.out.println("View Submission: " + str);
    }

    private void logErr(String str) {

        System.err.println("View Submission: " + str);
    }

}
