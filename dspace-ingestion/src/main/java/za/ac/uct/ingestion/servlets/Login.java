package za.ac.uct.ingestion.servlets;

import za.ac.uct.ingestion.services.CollectionManager;
import za.ac.uct.ingestion.services.CookieManager;
import za.ac.uct.ingestion.storage.DatabaseController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Darryl Meyer
 * 09 September 2015
 * <p>
 * Handles user logins for the Ingestion Manager.
 */
@WebServlet(name = "Login")
public class Login extends HttpServlet {
    private String port;

    private String hostname;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");

        loadConnectionConfig();

        request.getSession().setAttribute("logout_success_title", null);
        request.getSession().setAttribute("logout_success_message", null);
        request.getSession().setAttribute("logout_failure_title", null);
        request.getSession().setAttribute("logout_failure_message", null);

        String email = request.getParameter("login_email");
        String password = request.getParameter("login_password");

        String token = login(email, password);

        // If the user could not be logged in the token will be null
        if (token != null) {
            // Load database to check the role of the user
            String resourcesPath = File.separator + "WEB-INF" + File.separator + "classes" + File.separator;
            String pathToDatabase = getServletContext().getRealPath(resourcesPath);
            DatabaseController database = new DatabaseController(pathToDatabase);

            List<String> mappings = database.getMappings();
            request.getSession().setAttribute("savedMappings", mappings);

            request.getSession().setAttribute("user_email", email);
            CookieManager.addCookie(response, "im_", token, 30000);

            // If the user is a manager they are redirected to the approve page, else they are redirected to the submit page
            if (database.isUserManager(email)) {
                // Build the components of the approve page
                String submissions = database.getAllPendingSubmissionsHTMLList();
                if (!submissions.equals("")){
                    request.getSession().setAttribute("pending_submissions", submissions);
                }

                // Set the manager session variable
                request.getSession().setAttribute("manager", true);
                request.getSession().setAttribute("user", false);
                RequestDispatcher dispatcher = request.getRequestDispatcher("/Approve.jsp");
                dispatcher.forward(request, response);
            } else {
                // Build the components of the submit batch page
                String collections = CollectionManager.getAllCollectionsString();
                request.getSession().setAttribute("collections", collections);

                // Pending submissions
                String submissionsPending = database.getUserPendingSubmissionsHTMLList(email);
                if (!submissionsPending.equals("")) {
                    request.getSession().setAttribute("user_pending_submissions", submissionsPending);
                }

                // Approved submissions
                String submissionsApproved = database.getUserApprovedSubmissionsHTMLList(email);
                if (!submissionsApproved.equals("")) {
                    request.getSession().setAttribute("user_approved_submissions", submissionsApproved);
                }

                // Set the user session variable
                request.getSession().setAttribute("user", true);
                request.getSession().setAttribute("manager", false);
                RequestDispatcher dispatcher = request.getRequestDispatcher("/Submit.jsp");
                dispatcher.forward(request, response);
            }
        } else {
            logErr("Error occurred during login.");
            request.getSession().setAttribute("title", "Login Unsuccessful.");
            request.getSession().setAttribute("message", "You could not be logged in. Check that you have entered the correct " +
                    "email and password combination.");
            RequestDispatcher dispatcher = request.getRequestDispatcher("/Error.jsp");
            dispatcher.include(request, response);
        }

    }

    /*
     * Redirect user if the attempt a GET.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendRedirect("/Login.jsp");
    }


    /*
     * Load the configuration file to connect to the rest api.
     */
    public void loadConnectionConfig() {

        ResourceBundle resourceBundle = ResourceBundle.getBundle("rest");
        hostname = resourceBundle.getString("rest.hostname");
        port = resourceBundle.getString("rest.port");
    }

    /*
     * Log the user into the localhost DSpace repository using the REST API.
     * <p>
     * Command line usage for the REST endpoint:  curl -H "Content-Type: application/json" --data '{"email":"email-address",
     * "password":"password"}' http://localhost:8080/rest/login
     *
     * @param email    User email address.
     * @param password User password.
     * @return Token for the user, null if there was an error during login.
     */
    public String login(String email, String password) {

        String serverResponse = null;

        // Try log the user in using the DSpace REST API login endpoint
        try {
            URL url = new URL("http://" + hostname + ":" + port + "/rest/login");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            String input = "{\"email\":\"" + email + "\", \"password\":\"" + password + "\"}";

            OutputStream os = conn.getOutputStream();
            os.write(input.getBytes());
            os.flush();

            if (conn.getResponseCode() != 200) {
                serverResponse = null;
                logErr("Login Failed: HTTP error code: " + conn.getResponseCode());
                logErr("Failed URL: " + url.toString());
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));

                StringBuilder response = new StringBuilder();
                String output;
                while ((output = br.readLine()) != null) {
                    response.append(output);
                }

                logln("Login Successful.");

                serverResponse = response.toString();

                conn.disconnect();
            }

        } catch (IOException e) {
            logErr("An error occurred when trying to log the user in using the REST api." + e.getMessage());
        }

        return serverResponse;
    }

    /*
     * Print to standard out log messages specific to the Login servlet.
     */
    private void logln(String str) {

        System.out.println("Login Servlet: " + str);
    }

    /*
     * Print to standard error out log messages specific to the Login servlet.
     */
    private void logErr(String str) {

        System.err.println("Login Servlet: " + str);
    }
}
