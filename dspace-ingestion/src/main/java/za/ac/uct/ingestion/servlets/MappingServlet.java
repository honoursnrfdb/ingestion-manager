package za.ac.uct.ingestion.servlets;

import za.ac.uct.ingestion.storage.DatabaseController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Craig Feldman on 2015-09-21.
 *
 * Extracts the mapping and stores it in an array
 */
@WebServlet(name = "mapping-generator")
@MultipartConfig
public class MappingServlet extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//logln("Mapping post started");

		// Stores the primary DC field where each index corresponds to the column at that index
		String[] primaryDC = request.getParameterValues("DC-primary");
		// Stores the secondary DC field where each index corresponds to the column at that index
		String[] secondaryDC = request.getParameterValues("DC-secondary");

		int numFields = primaryDC.length;

		//logln("primary: " + Arrays.toString(primaryDC));
		//logln("secondary: " + Arrays.toString(secondaryDC));

		String[] dcTitles = new String[numFields];
		for (int i = 0; i < numFields; ++i){
			// lowercase first letter
			dcTitles[i] = "dc." + Character.toLowerCase(primaryDC[i].charAt(0)) + primaryDC[i].substring(1);
			if (!secondaryDC[i].isEmpty()) {
				// Remove spaces and convert to camel case
				secondaryDC[i] = secondaryDC[i].replace(" ", "");
				dcTitles[i] += "." + Character.toLowerCase(secondaryDC[i].charAt(0)) + secondaryDC[i].substring(1);
			}
		}

		// Check if we must save the mapping
		String saveName= (String)request.getSession().getAttribute("saveMappingAs");
		if (saveName != null && !saveName.isEmpty()) {
			//logln("saving mapping as " + saveName);
			String resourcesPath = File.separator + "WEB-INF" + File.separator + "classes";
			String pathToDatabase = request.getServletContext().getRealPath(resourcesPath);
			DatabaseController database = new DatabaseController(pathToDatabase);
			database.insertMapping(saveName, dcTitles);
		}

		//logln(Arrays.toString(dcTitles));

		request.getSession().setAttribute("dcTitles", Arrays.toString(dcTitles));

		//logln("post finished");

		RequestDispatcher dispatcher = request.getRequestDispatcher("/upload");
		dispatcher.forward(request, response);
	}

	private void logln(String str) {
		System.out.println("Mapping Servlet: " + str);
	}

	private void logErr(String str) {
		System.err.println("Mapping Servlet: " + str);
	}
}
