package za.ac.uct.ingestion.storage;

import com.google.gson.Gson;
import za.ac.uct.ingestion.data.Submission;

import java.io.File;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Darryl Meyer
 * @author Craig Feldman
 *         11 September 2015
 *         <p>
 *         Controller class for the Ingestion Manager database.
 */
public class DatabaseController {

    // Name of the database
    final String databaseName = "ingestion-manager.db";
    // Root path of server where the database is located
    String rootServerPath = "";
    // Connection to database
    Connection connection;

    /**
     * Creates a new database object at the path passed to it.
     *
     * @param rootServerPath Path to create the database at.
     */
    public DatabaseController(String rootServerPath) {

        //logln("File.separator: " + File.separator);

        if (rootServerPath.substring(rootServerPath.length() - 1).equals(File.separator)) {
            this.rootServerPath = rootServerPath;
        } else {
            this.rootServerPath = rootServerPath + File.separator;
        }

        openDatabase();
    }

    // --- Database control methods ---

    /**
     * Opens the connection to the database.
     *
     * @return True if the connection was opened successfully, false otherwise.
     */
    public boolean openDatabase() {

        boolean status = false;

        try {
            Class.forName("org.sqlite.JDBC");

            // Connect to the database
            connection = DriverManager.getConnection("jdbc:sqlite:" + rootServerPath + databaseName);

            status = true;
            //logln("Database opened successfully. " + "jdbc:sqlite:" + rootServerPath + databaseName);
        } catch (Exception e) {
            logErr("Error opening database: " + e.getMessage());
        }

        return status;
    }

    /**
     * Closes the connection to the database.
     *
     * @return True if the connection was closed successfully, false otherwise.
     */
    public boolean closeDatabase() {

        boolean status = false;

        try {
            connection.close();

            status = true;
//            logln("Database closed successfully.");
        } catch (Exception e) {
            logErr("Error closing database: " + e.getMessage());
        }

        return status;
    }

    /**
     * Checks whether the connection to the database is closed.
     *
     * @return True if connection is closed, false otherwise.
     */
    public boolean isClosed() {

        boolean status = false;

        try {
            status = connection.isClosed();
        } catch (Exception e) {
            logErr("Error reading database closed state: " + e.getMessage());
        }

        return status;
    }

    // --- Executing SQL statement ---

    /*
     * Connects to the database and performs the SQL update statement passed to it.
     * @param sql The SQL update statement to perform.
     * @return True if the update was performed successfully.
     */
    private boolean executeSQLUpdate(String sql) {

        boolean queryStatus = false;

        if (isClosed()) {
            openDatabase();
        }

        try {
            // Prepare SQL Update statement
            connection.createStatement().executeUpdate(sql);

            queryStatus = true;
        } catch (Exception e) {
            logErr("Error executing SQL update: " + e.getMessage() + "\nSQL: " + sql);
        }

        return queryStatus;
    }

    // --- Specific function methods ---

    /**
     * Creates the user role table if it does not already exist.
     *
     * @return True if the user role table is crated successfully.
     */
    public boolean createUserRoleTable() {

        boolean status;

        // Create the User Role table
        String sql = "CREATE TABLE IF NOT EXISTS im_user_role (ID INTEGER PRIMARY KEY, email TEXT NOT NULL, role TEXT NOT " +
                "NULL)";
        status = executeSQLUpdate(sql);

        return status;
    }

    /**
     * Creates the submissions table if it does not already exist.
     *
     * @return True if the table was created successfully.
     */
    public boolean createSubmissionsTable() {

        boolean status;

        // Create the User Role table
        String sql = "CREATE TABLE IF NOT EXISTS im_submissions (ID INTEGER PRIMARY KEY, email TEXT NOT NULL, collection_name " +
                "TEXT NOT NULL, collection_id TEXT NOT NULL, status TEXT NOT NULL, filename TEXT NOT NULL, file_path TEXT " +
                "NOT NULL, date_added TEXT NOT NULL, date_approved TEXT NOT NULL, dc_titles TEXT NOT NULL, separator TEXT NOT " +
                "NULL, has_header TEXT NOT NULL)";
        status = executeSQLUpdate(sql);

        return status;
    }

    //*******************************************
    //  MAPPING TABLE METHODS
    //*******************************************

    /**
     * Creates the mapping storage table if it does not already exist.
     *
     * @return True if the table was created successfully.
     */
    private boolean createMappingsTable() {

        boolean status, statusInsert;

        // Create the User Role table
        String sql = "CREATE TABLE IF NOT EXISTS im_mappings (mapping_name PRIMARY KEY, dc_titles TEXT NOT NULL)";
        status = executeSQLUpdate(sql);

        statusInsert = executeSQLUpdate(sql);

        return status && statusInsert;
    }

    /**
     * Save a mapping into the database.
     *
     * @param mappingName the name to save the mapping as.
     * @param dcTitles    an array containing the mappings.
     * @return true if successful
     */
    public boolean insertMapping(String mappingName, String[] dcTitles) {

        int status;
        // Check if table exists
        if (!tableExists("im_mappings")) {
            createMappingsTable();
        }

        // Serialise array
        Gson gson = new Gson();
        String jsonArray = gson.toJson(dcTitles);

        PreparedStatement stmt = null;
        try {
            if (isClosed())
                openDatabase();
            stmt = connection.prepareStatement("INSERT OR REPLACE INTO im_mappings (mapping_name, dc_titles) VALUES (?, ?)");
            stmt.setString(1, mappingName);
            stmt.setString(2, jsonArray);
            status = stmt.executeUpdate();
        } catch (SQLException e) {
            logErr("Error inserting new mapping. " + e.getMessage());
            return false;
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logErr("Error closing prepared statement when saving mapping. " + e.getMessage());
            }
        }
        if (status >= 1)
            logln("Successfully saved new mapping: '" + mappingName + "' = " + Arrays.toString(dcTitles));
        else
            logErr("Failed to save mapping: '" + mappingName + "' = " + Arrays.toString(dcTitles));

        //boolean status;
        // String sql = "INSERT OR REPLACE INTO im_mappings (mapping_name, dc_titles) " + "VALUES ('" + mappingName + "', '" +
        // jsonArray + "');";
        // status = executeSQLUpdate(sql);
        //return status

        return status >= 1;
    }

    /**
     * Returns specified mapping
     *
     * @param mappingName the name of the mapping
     * @return an array containing the DC titles
     */
    public String[] getMapping(String mappingName) {

        ResultSet resultSet = null;
        String[] toReturn = null;

        PreparedStatement stmt = null;
        try {
            if (isClosed())
                openDatabase();
            stmt = connection.prepareStatement("SELECT * FROM im_mappings WHERE mapping_name = ?");
            stmt.setString(1, mappingName);
            resultSet = stmt.executeQuery();

            // Get the DC titles
            resultSet.next();

            // deserialise the result
            String value = resultSet.getString("dc_titles");
            Gson gson = new Gson();
            toReturn = gson.fromJson(value, String[].class);

        } catch (SQLException e) {
            logErr("Error getting mapping. " + e.getMessage());
        } finally {
            try {
                if (stmt != null)
                    stmt.close();

                if (resultSet != null)
                    resultSet.close();

            } catch (Exception e) {
                logErr("Error closing prepared statement or result set when getting mapping. " + e.getMessage());
            }
        }

        /*
        try {
            // Select SQL statement
            String sql = "SELECT * FROM im_mappings WHERE mapping_name = " +
                    "'" + id + "';";
            // Prepare SQL Select statement
            resultSet = connection.createStatement().executeQuery(sql);

            // Loop through all results of the query
            resultSet.next();

            // deserialise the result
            String value = resultSet.getString("dc_titles");
            Gson gson = new Gson();
            toReturn = gson.fromJson(value, String[].class);

            resultSet.close();

        } catch (Exception e) {
            logErr("Error getting mapping name " + id + " from database: " + e.getMessage());
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }
       // logln("returning " + Arrays.toString(toReturn));
       */

        logln("Returning saved mapping: '" + mappingName + "' = " + Arrays.toString(toReturn));
        return (toReturn);
    }

    /**
     * Returns all the mapping names
     *
     * @return a list containing the mapping names
     */
    public List<String> getMappings() {

        ResultSet resultSet = null;
        List<String> toReturn = null;
        if (isClosed()) {
            openDatabase();
        }

        if (!tableExists("im_mappings")) {
            createMappingsTable();
        }

        try {
            // Select SQL statement
            String sql = "SELECT mapping_name FROM im_mappings;";
            // Prepare SQL Select statement
            resultSet = connection.createStatement().executeQuery(sql);

            toReturn = new ArrayList<String>();
            // Loop through all results of the query
            while (resultSet.next()) {
                toReturn.add(resultSet.getString("mapping_name"));
            }

            resultSet.close();
        } catch (Exception e) {
            logErr("Error getting mapping names from database: " + e.getMessage());
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }

        return toReturn;
    }

    /**
     * Deletes a saved mapping.
     * Currently not used.
     *
     * @param name the name of the mapping to delete
     * @return true if successful
     */
    public boolean removeMapping(String name) {

        boolean status = false;

        if (isClosed()) {
            openDatabase();
        }

        // Check if table exists
        if (tableExists("im_mappings")) {

            String sql = "DELETE FROM im_mappings WHERE mapping_name = '" + name + "';";
            status = executeSQLUpdate(sql);
        }

        return status;
    }

    /**
     * Deletes all saved mappings
     * Currentl not used
     *
     * @return true if successful
     */
    public boolean removeAllMappings() {

        boolean status = false;

        if (isClosed()) {
            openDatabase();
        }

        // Check if table exists
        if (tableExists("im_mappings")) {

            String sql = "DELETE FROM im_mappings;";
            status = executeSQLUpdate(sql);
        }

        return status;
    }


    /**
     * Checks to see if the table exists in the database.
     *
     * @param tableName The name of the table to check.
     * @return True if the table exists, false otherwise.
     */
    public boolean tableExists(String tableName) {

        if (isClosed()) {
            openDatabase();
        }

        boolean tableExists = false;
        ResultSet resultSet = null;
        PreparedStatement statement;

        try {
            // Prepare SQL Select statement
            statement = connection.prepareStatement("SELECT name FROM sqlite_master WHERE type = 'table' AND name = ?");
            statement.setString(1, tableName);
            resultSet = statement.executeQuery();

            String name = resultSet.getString("name");

            resultSet.close();

            if (name.equals(tableName)) {
                tableExists = true;
            }
        } catch (SQLException e) {
            logErr("Could not get table name from database. " + e.getMessage());
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }

        return tableExists;
    }

    /**
     * Inserts a new submission into the database.
     * Status is by default pending for new submissions.
     *
     * @param submission The submission object to store in the database.
     * @return True if the submission was entered successfully, false otherwise.
     */
    public boolean insertSubmission(Submission submission) {

        boolean status = false;

        // Check if table exists
        if (!tableExists("im_submissions")) {
            createSubmissionsTable();
        }

        PreparedStatement statement = null;

        if (isClosed()) {
            openDatabase();
        }

        try {
            // Prepare SQL Update statement
            statement = connection.prepareStatement("INSERT OR REPLACE INTO im_submissions (email, collection_name, " +
                    "collection_id, status, filename, " +
                    "file_path, date_added, date_approved, dc_titles, separator, has_header) VALUES (?, ?, ?, 'pending', " +
                    "?, ?, ?, ?, ?, ?, ?);");
            statement.setString(1, submission.getUserEmail());
            statement.setString(2, submission.getCollectionName());
            statement.setString(3, submission.getCollectionID());
            statement.setString(4, submission.getFilename());
            statement.setString(5, submission.getMetadataFileAbsolutePath());
            statement.setString(6, submission.getDateAdded());
            statement.setString(7, submission.getDateApproved());
            statement.setString(8, submission.getDcTitles());
            statement.setString(9, submission.getSeparator());
            statement.setString(10, submission.getHasHeader());
            statement.executeUpdate();

            status = true;
        } catch (Exception e) {
            logErr("Error executing SQL update: " + e.getMessage());
        }
        logln("Has the submission been successful?: " + status);

        return status;
    }

    public boolean approveSubmission(String ID) {

        boolean status = false;
        if (setSubmissionStatusApproved(ID) && setSubmissionDateApproved(ID)) {
            status = true;
        }
        return status;
    }

    /**
     * Sets the submission status for the specific submission to approved.
     *
     * @param ID Unique ROWID for the specific submission
     * @return True if the status was changed successfully to approved, false otherwise.
     */
    private boolean setSubmissionStatusApproved(String ID) {

        boolean status = false;

        // Check if table exists
        if (tableExists("im_submissions")) {
            PreparedStatement statement = null;
            if (isClosed()) {
                openDatabase();
            }

            try {
                // Prepare SQL Update statement
                statement = connection.prepareStatement("UPDATE im_submissions SET status = 'approved' WHERE ID = ?;");
                statement.setString(1, ID);
                statement.executeUpdate();

                status = true;
            } catch (Exception e) {
                logErr("Error executing SQL update: " + e.getMessage());
            }
        }

        return status;
    }

    /**
     * Sets the submission date approved for the specific submission to approved.
     *
     * @param ID Unique ROWID for the specific submission
     * @return True if the status was changed successfully to approved, false otherwise.
     */
    private boolean setSubmissionDateApproved(String ID) {

        boolean status = false;

        // Check if table exists
        if (tableExists("im_submissions")) {

            // Set the date approved value
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            String dateApproved = dateFormat.format(date);
            PreparedStatement statement = null;

            if (isClosed()) {
                openDatabase();
            }

            try {
                // Prepare SQL Update statement
                statement = connection.prepareStatement("UPDATE im_submissions SET date_approved = ? WHERE ID = ?;");
                statement.setString(1, dateApproved);
                statement.setString(2, ID);
                statement.executeUpdate();

                status = true;
            } catch (Exception e) {
                logErr("Error executing SQL update: " + e.getMessage());
            }
        }

        return status;
    }

    /**
     * Returns a submission object for the associated row ID.
     *
     * @param rowID The submission's unique row ID.
     * @return The submission object.
     */
    public Submission selectSubmission(String rowID) {

        ResultSet resultSet = null;
        PreparedStatement statement = null;
        // Initialize variables
        String email = null;
        String collectionID = null;
        String collectionName = null;
        String filePath = null;
        String dateAdded = null;
        String dateApproved = null;
        String dcTitles = null;
        String separator = null;
        String hasHeader = null;

        if (isClosed()) {
            openDatabase();
        }

        try {
            // Prepare SQL Select statement
            statement = connection.prepareStatement("SELECT email, collection_id, collection_name, file_path, date_added, " +
                    "date_approved, dc_titles, " +
                    "separator, has_header FROM im_submissions WHERE ID = ?;");
            statement.setString(1, rowID);
            resultSet = statement.executeQuery();

            // Initialize variables
            email = resultSet.getString("email");
            collectionID = resultSet.getString("collection_id");
            collectionName = resultSet.getString("collection_name");
            filePath = resultSet.getString("file_path");
            dateAdded = resultSet.getString("date_added");
            dateApproved = resultSet.getString("date_approved");
            dcTitles = resultSet.getString("dc_titles");
            separator = resultSet.getString("separator");
            hasHeader = resultSet.getString("has_header");

            resultSet.close();
        } catch (Exception e) {
            logErr("Error getting submission from database: " + e.getMessage());
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }

        return new Submission(filePath, email, collectionID, collectionName, dateAdded, dateApproved, dcTitles, separator,
                hasHeader);
    }

    /**
     * Returns a HTML formatted list of the pending submissions.
     *
     * @return HTML formatted list of pending submissions.
     */
    public String getAllPendingSubmissionsHTMLList() {

        StringBuilder submissions = new StringBuilder();

        ResultSet resultSet = null;
        if (isClosed()) {
            openDatabase();
        }
        try {
            // Select SQL statement
            String sql = "SELECT ID, email, collection_name, filename FROM im_submissions WHERE status = 'pending';";
            // Prepare SQL Select statement
            resultSet = connection.createStatement().executeQuery(sql);

            // Initialize variables
            String ID = "";
            String email = "";
            String collectionName = "";
            String filename = "";

            // Loop through all results of the query
            while (resultSet.next()) {
                ID = resultSet.getString("ID");
                email = resultSet.getString("email");
                collectionName = resultSet.getString("collection_name");
                filename = resultSet.getString("filename");

                submissions.append("\t\t<tr>\n");
                submissions.append("\t\t\t<td class=\"evenRowOddCol\">\n");
                submissions.append("\t\t\t\t<form action=\"/ingest/view\" method=\"post\">\n");
                submissions.append("\t\t\t\t\t<input name=\"submission_id\" value=\"").append(ID).append("\" type=\"hidden\">\n");
                submissions.append("\t\t\t\t\t<input class=\"btn btn-default\" name=\"submit_open\" value=\"View " +
                        "Details\" type=\"submit\">\n");
                submissions.append("\t\t\t\t</form>\n");
                submissions.append("\t\t\t</td>\n");
                submissions.append("\t\t\t<td headers=\"t11\" class=\"evenRowEvenCol\">\n");
                submissions.append("\t\t\t\t<form onsubmit=\"return confirm('Are you sure you want to approve " +
                        "this submission?');\" action=\"/ingest/approve\" method=\"post\">\n");
                submissions.append("\t\t\t\t\t<input name=\"submission_id\" value=\"").append(ID).append("\" type=\"hidden\">\n");
                submissions.append("\t\t\t\t\t<input class=\"btn btn-success\" name=\"submit_approve\" value=\"Approve\" " +
                        "type=\"submit\">\n");
                submissions.append("\t\t\t\t</form>\n");
                submissions.append("\t\t\t</td>\n");
                submissions.append("\t\t\t<td headers=\"t12\" class=\"evenRowOddCol\"><a href=\"mailto:").append(email).append
                        ("?subject=Your submission ").append(filename).append(" to the DSpace Ingestion Manager\">").append
                        (email).append("</a></td>\n");
                submissions.append("\t\t\t<td headers=\"t13\" class=\"evenRowEvenCol\">").append(filename).append("</td>\n");
                submissions.append("\t\t\t<td headers=\"t14\" class=\"evenRowOddCol\">").append(collectionName).append("</td>\n");
                submissions.append("\t\t\t<td headers=\"t15\" class=\"evenRowEvenCol\">\n");
                submissions.append("\t\t\t\t<form onsubmit=\"return confirm('Are you sure you want to remove this " +
                        "submission?');\" action=\"/ingest/remove\" method=\"post\">\n");
                submissions.append("\t\t\t\t\t<input name=\"submission_id\" value=\"").append(ID).append("\" type=\"hidden\">\n");
                submissions.append("\t\t\t\t\t<input class=\"btn btn-danger\" name=\"submit_delete\" value=\"Remove\" " +
                        "type=\"submit\">\n");
                submissions.append("\t\t\t\t</form>\n");
                submissions.append("\t\t\t</td>\n");
                submissions.append("\t\t</tr>\n");
            }

            resultSet.close();
        } catch (Exception e) {
            logErr("Error getting all submissions array from database: " + e.getMessage());
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }

        return submissions.toString();
    }

    /**
     * Returns an HTML formatted list of a specific users pending submissions.
     *
     * @param email The email address for the specific user.
     * @return The HTML formatted list.
     */
    public String getUserPendingSubmissionsHTMLList(String email) {

        StringBuilder submissions = new StringBuilder();

        ResultSet resultSet = null;
        PreparedStatement statement = null;
        if (isClosed()) {
            openDatabase();
        }

        try {
            // Select SQL statement
            String sql = "SELECT ID, email, collection_name, filename FROM im_submissions WHERE status = 'pending' AND email = " +
                    "'" + email + "';";
            // Prepare SQL Select statement
            statement = connection.prepareStatement("SELECT ID, email, collection_name, filename FROM im_submissions WHERE " +
                    "status = 'pending' AND email = ?;");
            statement.setString(1, email);
            resultSet = statement.executeQuery();

            // Initialize variables
            String ID = "";
            String collectionName = "";
            String filename = "";


            // Loop through all results of the query
            while (resultSet.next()) {
                ID = resultSet.getString("ID");
                collectionName = resultSet.getString("collection_name");
                filename = resultSet.getString("filename");

                submissions.append("<tr>\n");
                submissions.append("\t<td class=\"evenRowOddCol\">\n");
                submissions.append("\t\t<form action=\"/ingest/view\" method=\"post\">\n");
                submissions.append("\t\t\t<input name=\"submission_id\" value=\"").append(ID).append("\" type=\"hidden\">\n");
                submissions.append("\t\t\t<input class=\"btn btn-default\" name=\"submit_open\" value=\"View Details\" " +
                        "type=\"submit\">\n");
                submissions.append("\t\t</form>\n");
                submissions.append("\t</td>\n");
                submissions.append("\t<td headers=\"t11\" class=\"evenRowOddCol\">").append(filename).append("</td>\n");
                submissions.append("\t<td headers=\"t12\" class=\"evenRowEvenCol\">").append(collectionName).append("</td>\n");
                submissions.append("\t<td headers=\"t13\" class=\"evenRowOddCol\">\n");
                submissions.append("\t\t<form onsubmit=\"return confirm(\'Are you sure you want to remove this submission?\');" +
                        "\" action=\"/ingest/remove\" method=\"post\">\n");
                submissions.append("\t\t\t<input name=\"submission_id\" value=\"").append(ID).append("\" type=\"hidden\">\n");
                submissions.append("\t\t\t<input class=\"btn btn-danger\" name=\"submit_delete\" value=\"Remove\" " +
                        "type=\"submit\">\n");
                submissions.append("\t\t</form>\n");
                submissions.append("\t</td>\n");
                submissions.append("</tr>\n");
            }

            resultSet.close();
        } catch (Exception e) {
            logErr("Error getting user pending submissions array from database: " + e.getMessage());
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }

        return submissions.toString();
    }

    /**
     * Returns an HTML formatted list of the approved submissions for the specifc user.
     *
     * @param email The email address for the user.
     * @return The HTML formatted list.
     */
    public String getUserApprovedSubmissionsHTMLList(String email) {

        StringBuilder submissions = new StringBuilder();

        ResultSet resultSet = null;
        PreparedStatement statement = null;
        if (isClosed()) {
            openDatabase();
        }

        try {
            // Select SQL statement
            // Prepare SQL Select statement
            statement = connection.prepareStatement("SELECT ID, email, collection_name, filename FROM im_submissions WHERE " +
                    "status = 'approved' AND email = ?;");
            statement.setString(1, email);
            resultSet = statement.executeQuery();

            // Initialize variables
            String ID = "";
            String collectionName = "";
            String filename = "";

            // Loop through all results of the query
            while (resultSet.next()) {
                ID = resultSet.getString("ID");
                collectionName = resultSet.getString("collection_name");
                filename = resultSet.getString("filename");

                submissions.append("\t\t<tr>\n");
                submissions.append("\t\t\t<td class=\"evenRowOddCol\">\n");
                submissions.append("\t\t\t\t<form action=\"/ingest/view\" method=\"post\">\n");
                submissions.append("\t\t\t\t\t<input name=\"submission_id\" value=\"").append(ID).append("\" type=\"hidden\">\n");
                submissions.append("\t\t\t\t\t<input class=\"btn btn-default\" name=\"submit_open\" value=\"View Details\" " +
                        "type=\"submit\">\n");
                submissions.append("\t\t\t\t</form>\n");
                submissions.append("\t\t\t</td>\n");
                submissions.append("\t\t\t<td headers=\"t21\" class=\"evenRowOddCol\">").append(filename).append("</td>\n");
                submissions.append("\t\t\t<td headers=\"t22\" class=\"evenRowEvenCol\">").append(collectionName).append
                        ("</td>\n");
                submissions.append("\t\t</tr>\n");
            }

            resultSet.close();
        } catch (Exception e) {
            logErr("Error getting user approved submissions array from database: " + e.getMessage());
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }

        return submissions.toString();
    }

    /**
     * Removes the submission from the database that corresponds to the row ID.
     *
     * @param ID The submission's row ID.
     * @return True if the submission was successfully removed, false otherwise.
     */
    public boolean removeSubmission(String ID) {

        boolean status = false;

        if (isClosed()) {
            openDatabase();
        }

        // Check if table exists
        if (tableExists("im_submissions")) {
            PreparedStatement statement = null;
            try {
                // Prepare SQL Update statement
                statement = connection.prepareStatement("DELETE FROM im_submissions WHERE ID = ?;");
                statement.setString(1, ID);
                statement.executeUpdate();

                status = true;
            } catch (Exception e) {
                logErr("Error executing SQL update: " + e.getMessage());
            }
        }

        return status;
    }

    /**
     * Checks to see in the user is the owner of the submission the row ID.
     *
     * @param ID    Row ID of the submission.
     * @param email THe email address of the user.
     * @return True if they are the owner, false otherwise.
     */
    public boolean isUserOwner(String ID, String email) {

        boolean status = false;

        if (isClosed()) {
            openDatabase();
        }

        // Check if table exists
        if (tableExists("im_submissions")) {
            ResultSet resultSet = null;
            PreparedStatement statement = null;

            try {
                // Prepare SQL Select statement
                statement = connection.prepareStatement("SELECT ID, email FROM im_submissions WHERE ID = ? AND email = ?;");
                statement.setString(1, ID);
                statement.setString(2, email);
                resultSet = statement.executeQuery();

                String checkID = resultSet.getString("ID");
                String checkEmail = resultSet.getString("email");

                resultSet.close();

                if (checkEmail.equals(email) && checkID.equals(ID)) {
                    status = true;
                }
            } catch (SQLException e) {
                logErr("User could not be determined to be the owner. " + e.getMessage());
            } finally {
                if (resultSet != null) {
                    try {
                        resultSet.close();
                    } catch (SQLException ignored) {
                    }
                }
            }
        }

        return status;
    }

    /**
     * Returns the role of the user.
     *
     * @param email The email address for the user.
     * @return The role of the user.
     */
    public String checkUserRole(String email) {

        String role = "user";

        if (isClosed()) {
            openDatabase();
        }

        // Check if table exists
        if (!tableExists("im_user_role")) {
            createUserRoleTable();
        }

        String sql = ";";
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement("SELECT role FROM im_user_role WHERE email = ?;");
            statement.setString(1, email);
            resultSet = statement.executeQuery();

            role = resultSet.getString("role");

            resultSet.close();
        } catch (SQLException e) {
            logErr("Error checking user role. email: " + email + ", " + e.getMessage());
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }
        }

        return role;
    }

    /**
     * Checks to see if the user is a manager.
     *
     * @param email Email address for the user.
     * @return True if the user if a manager, false otherwise.
     */
    public boolean isUserManager(String email) {

        String role = checkUserRole(email);

        return role.equals("manager");
    }


    // --- Logging methods ---

    /*
     * Print to standard out log messages specific to the Database Controller.
     */
    private void logln(String str) {

        System.out.println("Database Controller: " + str);
    }

    /*
     * Print to standard error out log messages specific to the Database Controller.
     */
    private void logErr(String str) {

        System.err.println("Database Controller: " + str);
    }
}
