package za.ac.uct.ingestion.services;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Darryl Meyer
 * 09 September 2015
 * <p>
 * This class contains methods that are used to fetch the list of collections in the localhost DSpace repository.
 */
public class CollectionManager {

    private static String port;

    private static String hostname;

    /**
     * Fetches the list of collections and communities in the DSpace repository and formats the list into the correct HTML
     * formatting to be displayed in a drop down selection menu.
     *
     * @return
     */
    public static String getAllCollectionsString() {

        loadConnectionConfig();

        // First interface with the DSpace REST API to fetch an XML of collection and community data
        String collectionsXMLString = getAllCollectionsXMLString();

        if (collectionsXMLString != null) {
            // Second convert the XML string into an Array List of Community and Collection pairs
            ArrayList<String> collectionsArray = getCollectionsArray(collectionsXMLString);

            // Finally return a HTML string formatted for a drop down menu.
            return buildCollectionList(collectionsArray);
        } else {
            return "Could not get list of collections.";
        }
    }

    /*
 * Load the configuration file to connect to the rest api.
 */
    private static void loadConnectionConfig() {

        ResourceBundle resourceBundle = ResourceBundle.getBundle("rest");
        hostname = resourceBundle.getString("rest.hostname");
        port = resourceBundle.getString("rest.port");
    }


    /**
     * Interfaces with the localhost DSpace REST API to return a XML string of all collection and community data.
     * <p>
     * Command line usage for the REST endpoint: curl -X GET http://localhost:8080/rest/communities?expand=parentCommunity,
     * collections
     *
     * @return XML String of all collections and communities data.
     */
    private static String getAllCollectionsXMLString() {

        String xmlResponse = null;

        // Try access the communities endpoint of the localhost DSpace REST API
        try {
            URL url = new URL("http://" + hostname + ":" + port + "/rest/communities?expand=parentCommunity,collections");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/xml");


            if (conn.getResponseCode() != 200) {
                logErr("Error accessing the communities REST endpoint. HTTP error code: " + conn.getResponseCode());
            } else {
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));

                StringBuilder response = new StringBuilder();
                String output;
                while ((output = br.readLine()) != null) {
                    response.append(output);
                }

                xmlResponse = response.toString();
                conn.disconnect();
            }

        } catch (Exception e) {
            logErr("Error accessing the communities REST endpoint. " + e.getMessage());
        }

        return xmlResponse;
    }

    /**
     * This method parses the XML string received from the REST API and returns an array list of community and collection pairs.
     *
     * @param collectionXMLString The XML string received from the REST endpoint.
     * @return Array list of string with collection, community pairs.
     */
    private static ArrayList<String> getCollectionsArray(String collectionXMLString) {

        ArrayList<String> collections = new ArrayList<String>();
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(collectionXMLString));

            Document doc = builder.parse(is);
            doc.getDocumentElement().normalize();

            NodeList communityList = doc.getElementsByTagName("community");
            String community = "";

            // Loop for each node within the root node
            for (int temp1 = 0; temp1 < communityList.getLength(); temp1++) {

                Node communityNodes = communityList.item(temp1);

                if (communityNodes.getNodeType() == Node.ELEMENT_NODE) {

                    Element communityElement = (Element) communityNodes;

                    community = communityElement.getElementsByTagName("name").item(0).getTextContent();

                    NodeList collectionList = communityElement.getElementsByTagName("collections");
                    String collection = "";
                    String value = "";
                    String parentCommunity = null;

                    if (communityElement.getElementsByTagName("parentCommunity").getLength() > 0) {
                        NodeList parentCommunityList = communityElement.getElementsByTagName("parentCommunity");

                        Node parentCommunityNode = parentCommunityList.item(0);

                        if (parentCommunityNode.getNodeType() == Node.ELEMENT_NODE) {

                            Element parentCommunityElement = (Element) parentCommunityNode;

                            parentCommunity = parentCommunityElement.getElementsByTagName("name").item(0).getTextContent();
                        }
                    }

                    // Loop for each collection in the community node
                    for (int temp2 = 0; temp2 < collectionList.getLength(); temp2++) {

                        Node collectionsNode = collectionList.item(temp2);

                        if (collectionsNode.getNodeType() == Node.ELEMENT_NODE) {

                            Element collectionElement = (Element) collectionsNode;

                            collection = collectionElement.getElementsByTagName("name").item(0).getTextContent();
                            value = collectionElement.getElementsByTagName("id").item(0).getTextContent();

                            if (parentCommunity != null) {
                                collections.add(value + ":" + parentCommunity + " &gt; " + community + " &gt; " + collection);
                            } else {
                                collections.add(value + ":" + community + " &gt; " + collection);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            logErr("An error was caught when attempting to parse the XML string. " + e.getMessage());
        }
        return collections;
    }

    /**
     * Formats the array list of collection, community pairs into a HTML formatted list for a drop down menu.
     *
     * @param collections Array list of collection, community pairs.
     * @return HTML String for drop down menu of collections.
     */
    private static String buildCollectionList(ArrayList<String> collections) {
        /*
        <option value="1">Community 1 &gt; Collection 1</option>
        <option value="2">Community 2 &gt; Collection 1</option>
         */
        String htmlListItem;
        String[] listItemArray;
        String htmlList = "";

        if (collections.size() > 0) {
            for (String listItem : collections) {
                listItemArray = listItem.split(":");
                String value = listItemArray[0] + ":" + listItemArray[1];
                htmlListItem = "<option value=\"" + value + "\">" + listItemArray[1] + "</option>\n";
                htmlList += htmlListItem;
            }
        }

        return htmlList;
    }

    /*
     * Print to standard out log messages specific to the Collection Manager class.
     */
    private static void logln(String str) {

        System.out.println("Collection Manager: " + str);
    }


    /*
     * Print to standard out error log messages specific to the Collection Manager class.
     */
    private static void logErr(String str) {

        System.err.println("Collection Manager: " + str);
    }
}
