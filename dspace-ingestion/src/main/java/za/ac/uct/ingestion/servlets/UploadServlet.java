package za.ac.uct.ingestion.servlets;

import com.google.gson.Gson;
import com.opencsv.CSVReader;
import za.ac.uct.ingestion.mapping.ML;
import za.ac.uct.ingestion.mapping.MetadataMap;
import za.ac.uct.ingestion.services.Data;
import za.ac.uct.ingestion.storage.DatabaseController;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;

/**
 * @author Craig Feldman
 * Date created: 2015-07-21.
 */
@WebServlet(name = "metadata-mapping")
@MultipartConfig
public class UploadServlet extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		Part inputFilePart = request.getPart("file-upload"); // Retrieves <input type="file" name="file-upload">
		String inputFileName =  getSubmittedFileName(inputFilePart);

		// copy uploaded file to temp location
		String fileName = null;
		try (InputStream fileInputStream = inputFilePart.getInputStream()){
			fileName = copyFileToTempLocation(fileInputStream, inputFileName);
		} catch (IOException e) {
			logErr("Error copying file to server.\n" + e.getMessage());
		}

		// Some attributes that are needed whether or not a prior mapping is being used
		request.getSession().setAttribute("csvFileName", fileName);
		request.getSession().setAttribute("collectionID", request.getParameter("select-collection"));
		char separator = request.getParameter("separators").equals("\\t") ? '\t' : request.getParameter("separators").charAt(0) ;
		request.getSession().setAttribute("separator", separator);
		// Whether or not the first line of the CSV has a header or not
		boolean hasHeader = (request.getParameter("has-header") != null);
		request.getSession().setAttribute("hasHeader", hasHeader);

		// **********************
		//  PRIOR MAPPING
		// **********************
		String mappingFile = request.getParameter("load-mapping-file");
		if (!mappingFile.isEmpty()) {
			String resourcesPath = "/WEB-INF" + File.separator + "classes"  + File.separator;
			String pathToDatabase = getServletContext().getRealPath(resourcesPath);
			DatabaseController database = new DatabaseController(pathToDatabase);

			request.getSession().setAttribute("hasPriorMapping", true);

			String[] dcTitles = database.getMapping(mappingFile);
			//logln("retrieved " + Arrays.toString(dcTitles));

			// set dcTitles attribute
			request.getSession().setAttribute("dcTitles", Arrays.toString(dcTitles));

			request.getRequestDispatcher("/upload").forward(request, response);
			return;
		}

		// **********************
		//  NEW MAPPING
		// **********************

		// Load names from names database into a set to be used by ML
		Data.createNamesSet(getServletContext().getResourceAsStream("/WEB-INF/input_files/names.txt"));
		// Get InputStream to ML model
		InputStream modelFileInputStream = getServletContext().getResourceAsStream("/WEB-INF/input_files/J48V3.model");
		// Set the file used by ML for predictions
		ML.setClassifierFile(modelFileInputStream);

		MetadataMap mapper = new MetadataMap();

		String filePath = System.getProperty("java.io.tmpdir") + "/" + fileName;
		// Process the submitted file
		try (InputStream inputFile = new FileInputStream(filePath); CSVReader reader = new CSVReader(new InputStreamReader(inputFile), separator)){
			mapper.parseCSV(reader, hasHeader);
		} catch (Exception e) {
			logErr("An error occurred while parsing the file. Please check that it is a valid CSV file that conforms to the requirements of this application.");
		}

		// Create the percentage scores
		mapper.convertScoresToPercentage();
		// Set the header attribute tha contains column headings
		String[] header = mapper.getHeader();
		// Get the sorted results for each field
		List<List<Map.Entry<String, Integer>>> results = mapper.getSortedTracker();

		// the name to save the mapping as in the db (sanitised for HTML)
		request.getSession().setAttribute("saveMappingAs", escapeHtml4(request.getParameter("save-mapping-as")));
		request.getSession().setAttribute("mappingResults", results);
		request.getSession().setAttribute("numDCFields", Data.getNumDCFields());
		request.getSession().setAttribute("headerFields", header);
		// Some sample data for the fields (used by popover)
		request.getSession().setAttribute("sampleContent", mapper.getFormattedSampleContent());
		// the number of fields/columns in the data
		request.getSession().setAttribute("numFields", mapper.getNumFields());

		// Create a Json object for the secondary field map
		Gson gson = new Gson();
		String json = gson.toJson(Data.getSecondaryDCFields());
		request.getSession().setAttribute("secondaryDCFields", json);

		// Forward to to mapping page
		request.getRequestDispatcher("/Mapping.jsp").forward(request, response);
		//logln("post done");
	}

	/**
	 * Copies the uploaded file to temp location.
	 * @param inputStream inputStream to the file
	 * @param fileName the name of the submitted file
	 * @return the new filename with date and time appended to it
	 * @throws IOException
	 */
	private String copyFileToTempLocation(InputStream inputStream, String fileName) throws IOException {
		String tDir = System.getProperty("java.io.tmpdir");
		DateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd-HH-mm-ss");
		Calendar cal = Calendar.getInstance();
		fileName = fileName.replace(".csv", "-" + dateFormat.format(cal.getTime()) + ".csv");

		File newFile = new File(tDir + File.separator + fileName);
		Files.copy(inputStream, newFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
		//logln("Temp file created: " + newFile.getAbsolutePath().toString());

		return newFile.getName();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * If Servlet 3.1 cannot be used, we need to manually implement a utility method to get the submitted file name.
	 * Taken from: http://stackoverflow.com/questions/2422468/how-to-upload-files-to-server-using-jsp-servlet/
	 * @param part individual multipart form data item.
	 * @return the submitted file name.
	 */
	private static String getSubmittedFileName(Part part) {
		for (String cd : part.getHeader("content-disposition").split(";")) {
			if (cd.trim().startsWith("filename")) {
				String fileName = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
				return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1); // MSIE fix.
			}
		}
		return null;
	}

	private static void logln(String str) {
		System.out.println("File Upload Servlet: " + str);
	}

	private static void logErr(String str) {
		System.err.println("File Upload Servlet: " + str);
	}

}
