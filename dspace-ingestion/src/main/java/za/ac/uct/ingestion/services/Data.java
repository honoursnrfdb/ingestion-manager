package za.ac.uct.ingestion.services;

import weka.core.Attribute;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormatSymbols;
import java.util.*;

/**
 * @author Craig Feldman
 * Date Created: 2015-09-16
 *
 * Stores some data that is used by various aspects of the program.
 */
public class Data {

	// Stores a set containing month names
	private static final Set monthSet;
	// Change here if you want localised month names
	private static String[] longMonthNames = new DateFormatSymbols().getMonths();
	private static String[] shortMonthNames = new DateFormatSymbols().getShortMonths();
	// Stores names from a dictionary of names
	private static Set<String> nameSet;
	// Dublin Core Fields
	private static List<String> classVal = new ArrayList<>(
			Arrays.asList(
					"Contributor",
					"Coverage",
					"Creator",
					"Date",
					"Description",
					"Format",
					"Identifier",
					"Language",
					"Publisher",
					"Relation",
					"Rights",
					"Source",
					"Subject",
					"Title",
					"Type")
	);
	private static Map<String, String[]> secondaryDCFields;
	private static int numDCFields = classVal.size();
	// The attributes/features
	private static List<Attribute> attributeList = new ArrayList<>(
			Arrays.asList(
				new Attribute("number of characters"),
				new Attribute("number of words"),
				new Attribute("number of months"),
				new Attribute("number of person names"),
				new Attribute("% of digits"),
				new Attribute("% of letters"),
				new Attribute("% of non-alphanumeric"),
				new Attribute("% of capitals"),
				new Attribute("@@class@@",classVal))
			);

	// Create a set containing the month names
	static {
		monthSet = new HashSet<>();
		for (String monthName : longMonthNames)
			monthSet.add(monthName.toLowerCase());
		for (String monthName : shortMonthNames)
			monthSet.add(monthName.toLowerCase());
	}

	// Links secondary DC fields to primary
	static {
		secondaryDCFields = new HashMap<>();

		secondaryDCFields.put("Contributor", new String[] {
				"Advisor", "Author", "Editor", "Illustrator", "Other"
		});

		secondaryDCFields.put("Coverage", new String[] {
				"Spatial", "Temporal"
		});

		secondaryDCFields.put("Creator", new String[] {});

		secondaryDCFields.put("Date", new String[] {
				"Accessioned", "Available", "Copyright", "Created", "Issued", "Submitted"
		});

		secondaryDCFields.put("Description", new String[] {
				"Abstract", "Provenance", "Sponsorship", "Statement Of Responsibility", "Table Of Contents", "uri"
		});

		secondaryDCFields.put("Format", new String[] {
				"Extent", "Medium", "mime Type"
		});

		secondaryDCFields.put("Identifier", new String[] {
				"Citation", "govdoc", "isbn", "issn", "sici", "ismn", "Other", "uri"
		});

		secondaryDCFields.put("Language", new String[] {
				"iso"
		});

		secondaryDCFields.put("Publisher", new String[] {});

		secondaryDCFields.put("Relation", new String[] {
				"Has Part", "Has Version", "Is Based On", "Is Format Of", "Is Part Of", "Is Part Of Series", "Is Referenced By",
				"Is Replaced By", "Is Version Of", "Replaces", "Requires", "uri"
		});

		secondaryDCFields.put("Rights", new String[] {
				"uri"
		});

		secondaryDCFields.put("Source", new String[] {
				"uri"
		});

		secondaryDCFields.put("Subject", new String[] {
				"Classification", "ddc", "lcc", "lcsh", "mesh", "other"
		});

		secondaryDCFields.put("Title", new String[] {
				"Alternative"
		});

		secondaryDCFields.put("Type", new String[] {});
	}

	/** @return a map that links a DC-primary to available DC-secondary */
	public static Map<String, String[]> getSecondaryDCFields() {
		return secondaryDCFields;
	}

	/** Loads a set that contains names
	 * @param namesFileInputStream an InputStream to the names database file
	 */
	public static void createNamesSet (InputStream namesFileInputStream) {
		nameSet = new HashSet<>();
		try (BufferedReader br = new BufferedReader(new InputStreamReader(namesFileInputStream))) {
			String line;
			while ((line = br.readLine()) != null) {
				nameSet.add(line);
			}
		} catch (IOException e) {
			System.err.println(e.toString());
			System.err.println("Error accessing names database");
			System.err.println("Program will continue without names database. This is not recommended.");
		}
	}

	/** @return the number of DC fields */
	public static int getNumDCFields() {
		return numDCFields;
	}

	/** @return a set containing month names */
	public static Set getMonthSet() {
		return monthSet;
	}

	/** @return a set containing person names */
	public static Set<String> getNameSet() {
		return nameSet;
	}

	/** @return a list containing all the DC-primary fields */
	public static List<String> getDCFieldNames() {
		return classVal;
	}

	/** @return a list containing weka attributes */
	public static List<Attribute> getAttributeList() {
		return attributeList;
	}
}