package za.ac.uct.ingestion.servlets;

import org.apache.commons.io.FileUtils;
import za.ac.uct.ingestion.data.Submission;
import za.ac.uct.ingestion.storage.DatabaseController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

/**
 * Darryl Meyer
 * 11 September 2015
 * <p>
 * Handles the uploading of files to the webapp.
 */
@WebServlet(name = "Upload")
public class Upload extends HttpServlet {

    // Maximum upload file size (default 5MB)
    private static final int maxFileSize = 1024 * 1024 * 10;

    private File uploadedFile = null;
    private String collectionID = null;
    private String collectionName = null;
    private String userEmail = null;
    private String dateAdded = null;
    private String dcTitles = null;
    private String separator = null;
    private String hasHeader = null;

    /*
     * Verifies the the correct details for the submission have been set and redirects the user upon success or failure.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");

        // If the details of the submission are set correctly then begin with the submission.
        if (setSubmissionDetails(request)) {

            // Add submission to database
            String resourcesPath = File.separator + "WEB-INF" + File.separator + "classes"  + File.separator;
            String pathToDatabase = getServletContext().getRealPath(resourcesPath);
            DatabaseController database = new DatabaseController(pathToDatabase);

            Submission submission = new Submission(uploadedFile, userEmail, collectionID, collectionName, dateAdded,
                    "Pending Approval", dcTitles, separator, hasHeader);
            boolean submissionStatus = database.insertSubmission(submission);

            if (submissionStatus) {

                request.getSession().setAttribute("title", "Submission Successful.");
                request.getSession().setAttribute("message", "Your submission: " + submission.getFilename() + ", was successful" +
                        " and is now pending manager approval.");
                request.getSession().setAttribute("return_user", "Submit another batch");
                request.getSession().setAttribute("return_manager", null);
                response.sendRedirect("/ingest/Success.jsp");
            } else {
                request.getSession().setAttribute("title", "Submission Unsuccessful.");
                request.getSession().setAttribute("message", "Your submission could not be completed at this time.");
                request.getSession().setAttribute("return_user", "Try another submission");
                request.getSession().setAttribute("return_manager", null);
                response.sendRedirect("/ingest/Error.jsp");
            }

        } else {
            request.getSession().setAttribute("title", "Submission Unsuccessful.");
            request.getSession().setAttribute("message", "Your submission could not be completed at this time." +
                    " Ensure you have selected a file to upload and that you have selected the correct collection from the " +
                    "drop down list.");
            request.getSession().setAttribute("return_user", "Try another submission");
            request.getSession().setAttribute("return_manager", null);
            response.sendRedirect("/ingest/Error.jsp");
        }
    }

    /*
     * Sets the variables needed for the submission process
     */
    private boolean setSubmissionDetails(HttpServletRequest request) {

        boolean detailsSet = false;

        // Save the file in the upload directory
        String fileUploadPath = "upload";
        String rootServerPath = getServletContext().getRealPath("");
        String uploadPath = rootServerPath + File.separator + fileUploadPath;

        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        try {
            // Get the separator char of the csv file
            separator = request.getSession().getAttribute("separator").toString();

            // Get the name of the uploaded csv file
            String csvFileName = request.getSession().getAttribute("csvFileName").toString();

            File csvFile = new File(uploadDir, csvFileName);
            if (!csvFile.exists()) {
                csvFile.createNewFile();
            }

            File tempCSV = new File(System.getProperty("java.io.tmpdir"), csvFileName);
            logln("location of temp csv: " + tempCSV.getAbsolutePath());

            // Copy the csv file over to the upload directory
            FileUtils.copyFile(tempCSV, csvFile);

            // Set the uploaded file
            uploadedFile = csvFile;
            logln("location of saved csv: " + csvFile.getAbsolutePath());

            // Get the dcTitles string
            dcTitles = request.getSession().getAttribute("dcTitles").toString();

            // Get the user's email
            userEmail = request.getSession().getAttribute("user_email").toString();

            // Get the hasHeader attribute
            hasHeader = request.getSession().getAttribute("hasHeader").toString();

            // Get the collection ID and Name
            String[] collectionSelection =  request.getSession().getAttribute("collectionID").toString().split(":");
            collectionID = collectionSelection[0];
            collectionName = collectionSelection[1];

            // Set the date added value
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date date = new Date();
            dateAdded = dateFormat.format(date);

        } catch (Exception e) {
            logErr("Error occurred during submission. " + e.getMessage());
        }

        if (userEmail != null && uploadedFile != null && collectionID != null && dateAdded != null && collectionName != null
                && dcTitles != null && separator != null && hasHeader != null) {
            detailsSet = true;
        } else {
            logErr("One of; userEmail, uploadedFile, collectionID, dateAdded, collectionName, separator hasHeader or dcTitles " +
                    "are null.");
        }

        return detailsSet;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendRedirect("/Login.jsp");
    }

    /*
     * Print to standard out log messages specific to the upload servlet.
     */
    private void logln(String str) {

        System.out.println("Upload Servlet: " + str);
    }

    /*
     * Print to standard error out log messages specific to the upload servlet.
     */
    private void logErr(String str) {

        System.err.println("Upload Servlet: " + str);
    }
}
