package za.ac.uct.ingestion.servlets;

import za.ac.uct.ingestion.data.Submission;
import za.ac.uct.ingestion.storage.DatabaseController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Darryl Meyer
 * 15 September 2015
 * <p>
 * Serlvet to handle the removals of submissions.
 */
@WebServlet(name = "RemoveSubmission")
public class RemoveSubmission extends HttpServlet {

    /*
     * Coordinates the removals of submissions and redirects upon success or failure.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");

        // Load the database and user email session attribute
        String resourcesPath = File.separator + "WEB-INF" + File.separator + "classes" + File.separator;
        String pathToDatabase = getServletContext().getRealPath(resourcesPath);
        DatabaseController database = new DatabaseController(pathToDatabase);

        String email = request.getSession().getAttribute("user_email").toString();

        String ID = request.getParameter("submission_id");

        Submission submission = database.selectSubmission(ID);

        if (database.isUserManager(email)) {
            // Remove the submission from the database
            if (database.removeSubmission(ID)) {
                request.getSession().setAttribute("title", "Submission Removed.");
                request.getSession().setAttribute("message", "The submission: " + submission.getFilename() + ", was " +
                        "successfully" +
                        " removed.");
                request.getSession().setAttribute("return_user", null);
                request.getSession().setAttribute("return_manager", "Back to approve list");
                response.sendRedirect("/ingest/Success.jsp");
            } else {
                request.getSession().setAttribute("title", "Remove Unsuccessful.");
                request.getSession().setAttribute("message", "An error occurred and the submission could not be removed.");
                request.getSession().setAttribute("return_user", null);
                request.getSession().setAttribute("return_manager", "Back to approve list");
                response.sendRedirect("/ingest/Error.jsp");
            }
        } else if (database.isUserOwner(ID, email)) {
            // Remove the submission from the database
            if (database.removeSubmission(ID)) {
                request.getSession().setAttribute("title", "Submission Removed.");
                request.getSession().setAttribute("message", "Your submission: " +
                        submission.getFilename() + ", was successfully removed.");
                request.getSession().setAttribute("return_manager", null);
                request.getSession().setAttribute("return_user", "Back to submissions");
                response.sendRedirect("/ingest/Success.jsp");
            } else {
                request.getSession().setAttribute("title", "Remove Unsuccessful.");
                request.getSession().setAttribute("message", "Your submission could not be removed at this time.");
                request.getSession().setAttribute("return_manager", null);
                request.getSession().setAttribute("return_user", "Back to submissions");
                response.sendRedirect("/ingest/Error.jsp");
            }
        } else {
            request.getSession().setAttribute("title", "Remove Unsuccessful.");
            request.getSession().setAttribute("message", "You do not have permission to remove this submission.");
            request.getSession().setAttribute("return_manager", null);
            request.getSession().setAttribute("return_user", null);
            response.sendRedirect("/ingest/Error.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendRedirect("/Login.jsp");
    }
}
