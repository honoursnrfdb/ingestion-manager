package za.ac.uct.ingestion.servlets;

import za.ac.uct.ingestion.services.CollectionManager;
import za.ac.uct.ingestion.storage.DatabaseController;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Darryl Meyer
 * 14 September 2015
 * <p>
 * Updates the session attributes to the latest values before redirecting the user to the correct page
 */
@WebServlet(name = "Update")
public class Update extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("/Login.jsp");
    }

    /*
     * Redirects the manager or user to the correct page and updates any session attributes.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");

        // Load database to check the role of the user
        String resourcesPath = File.separator + "WEB-INF" + File.separator + "classes" + File.separator;
        String pathToDatabase = getServletContext().getRealPath(resourcesPath);
        DatabaseController database = new DatabaseController(pathToDatabase);

        String email = request.getSession().getAttribute("user_email").toString();

        // Reset session variables
        request.getSession().setAttribute("pending_submissions", null);
        request.getSession().setAttribute("user_pending_submissions", null);
        request.getSession().setAttribute("user_approved_submissions", null);
        request.getSession().setAttribute("collections", null);
        request.getSession().setAttribute("return_user", null);
        request.getSession().setAttribute("return_manager", null);
        request.getSession().setAttribute("submission_details", null);
        request.getSession().setAttribute("submission_preview", null);
        request.getSession().setAttribute("download_link", null);
        request.getSession().setAttribute("logout_success_title", null);
        request.getSession().setAttribute("logout_success_message", null);
        request.getSession().setAttribute("logout_failure_title", null);
        request.getSession().setAttribute("logout_failure_message", null);
        request.getSession().setAttribute("unsuccessful_submissions", null);
        request.getSession().setAttribute("unsuccessful_submissions_download", null);

        // If the user is a manager they are redirected to the approve page, else they are redirected to the submit page
        if (email != null && database.isUserManager(email)) {
            // Build the components of the approve page
            String submissions = database.getAllPendingSubmissionsHTMLList();
            if (!submissions.equals("")) {
                request.getSession().setAttribute("pending_submissions", submissions);
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("/Approve.jsp");
            dispatcher.forward(request, response);
        } else if (email != null) {
            // Build the components of the submit batch page
            String collections = CollectionManager.getAllCollectionsString();
            request.getSession().setAttribute("collections", collections);

            List<String> mappings = database.getMappings();
            request.getSession().setAttribute("savedMappings", mappings);

            // Pending submissions
            String submissionsPending = database.getUserPendingSubmissionsHTMLList(email);
            if (!submissionsPending.equals("")) {
                request.getSession().setAttribute("user_pending_submissions", submissionsPending);
            }

            // Approved submissions
            String submissionsApproved = database.getUserApprovedSubmissionsHTMLList(email);
            if (!submissionsApproved.equals("")) {
                request.getSession().setAttribute("user_approved_submissions", submissionsApproved);
            }

            RequestDispatcher dispatcher = request.getRequestDispatcher("/Submit.jsp");
            dispatcher.forward(request, response);
        } else {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/Login.jsp");
            dispatcher.forward(request, response);
        }
    }
}
