package za.ac.uct.ingestion.data;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Darryl Meyer
 * 11 September 2015
 * <p>
 * Object class for holding the details about a submission.
 */
public class Submission {

    private String userEmail;

    private String filename;

    private String metadataFileAbsolutePath;

    private String collectionID;

    private String collectionName;

    private String dateAdded;

    private String dateApproved;

    private String dcTitles;

    private String separator;

    private String hasHeader;

    public Submission(File metadataFile, String email, String collectionID, String collectionName, String dateAdded,
                      String dateApproved, String dcTitles, String separator, String hasHeader) {

        this.userEmail = email;
        this.filename = metadataFile.getName();
        this.metadataFileAbsolutePath = metadataFile.getAbsolutePath();
        this.collectionID = collectionID;
        this.collectionName = collectionName;
        this.dateAdded = dateAdded;
        this.dateApproved = dateApproved;
        this.dcTitles = dcTitles;
        this.separator = separator;
        this.hasHeader = hasHeader;
    }

    public Submission(String metadataFilePath, String email, String collectionID, String collectionName, String dateAdded,
                      String dateApproved, String dcTitles, String separator, String hasHeader) {

        this.userEmail = email;
        Path path = Paths.get(metadataFilePath);
        this.filename = path.getFileName().toString();
        this.metadataFileAbsolutePath = metadataFilePath;
        this.collectionID = collectionID;
        this.collectionName = collectionName;
        this.dateAdded = dateAdded;
        this.dateApproved = dateApproved;
        this.dcTitles = dcTitles;
        this.separator = separator;
        this.hasHeader = hasHeader;
    }

    public String getCollectionName() {

        return collectionName;
    }

    public String getCollectionID() {

        return collectionID;
    }

    public String getFilename() {

        return filename;
    }

    public String getMetadataFileAbsolutePath() {

        return metadataFileAbsolutePath;
    }

    public String getUserEmail() {

        return userEmail;
    }

    public String getDateAdded() {

        return dateAdded;
    }

    public String getDateApproved() {

        return dateApproved;
    }

    public String getDcTitles() {

        return dcTitles;
    }

    public String getSeparator() {

        return separator;
    }

    public String getHasHeader() {

        return hasHeader;
    }

    public boolean hasHeader() {
        return Boolean.parseBoolean(hasHeader);
    }

    public String toString() {

        StringBuilder submissionString = new StringBuilder();
        submissionString.append("Submission: ");
        submissionString.append(userEmail);
        submissionString.append(", ");
        submissionString.append(filename);
        submissionString.append(", ");
        submissionString.append(metadataFileAbsolutePath);
        submissionString.append(", ");
        submissionString.append(collectionID);
        submissionString.append(", ");
        submissionString.append(collectionName);
        submissionString.append(", ");
        submissionString.append(dateAdded);
        submissionString.append(", ");
        submissionString.append(dateApproved);
        submissionString.append(", ");
        submissionString.append(dcTitles);
        submissionString.append(", ");
        submissionString.append(separator);
        submissionString.append(", ");
        submissionString.append(hasHeader);
        return submissionString.toString();
    }
}
