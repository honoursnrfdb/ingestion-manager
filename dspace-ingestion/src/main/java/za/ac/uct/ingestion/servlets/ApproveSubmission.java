package za.ac.uct.ingestion.servlets;

import com.opencsv.CSVReader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import za.ac.uct.ingestion.data.Submission;
import za.ac.uct.ingestion.services.CookieManager;
import za.ac.uct.ingestion.storage.DatabaseController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Darryl Meyer
 * 15 September 2015
 * <p>
 * Serlvet to handle the approvals of submissions.
 */
@WebServlet(name = "ApproveSubmission")
public class ApproveSubmission extends HttpServlet {

    // The user's login token returned form the DSpace REST api
    private String token;

    // Array of unsuccessful single submissions
    private ArrayList<String> unsuccessfulSubmissions;

    // Count of the total number of submissions
    private int submissionCount;

    // CSV file separator
    private char separator;

    // Time spent of errors
    private long errorTime = 0;

    private String port;

    private String hostname;

    private boolean hasHeader;

    /*
     * Coordinates the approvals of submissions and redirects upon success or failure.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        loadConnectionConfig();

        // Performance test start time
        long startTime = System.currentTimeMillis();

        response.setContentType("text/html");

        String resourcesPath = File.separator + "WEB-INF" + File.separator + "classes" + File.separator;
        String pathToDatabase = getServletContext().getRealPath(resourcesPath);
        DatabaseController database = new DatabaseController(pathToDatabase);

        // Retrieve the user email from the session attribute
        String email = request.getSession().getAttribute("user_email").toString();
        String submissionID = request.getParameter("submission_id");

        // Check if user is manager
        if (database.isUserManager(email)) {
            // Load managers cookie to retrieve token
            token = CookieManager.getCookieValue(request, "im_");

            // Read details about submission from database
            Submission submission = database.selectSubmission(submissionID);

            if (token != null && submission != null) {
                hasHeader = submission.hasHeader();
                boolean processed = processCSV(submission);
                logln("Batch processed? " + processed + ", Total single " +
                        "submissions: " + submissionCount + ", Unsuccessful single " +
                        "submissions: " + unsuccessfulSubmissions.size());
                // Performance test end time
                long endTime = System.currentTimeMillis();
                logln("Performance Test - time to submit batch: " + (endTime - startTime) + " ms.");
                logln("Performance Test - time spent on errors: " + errorTime + " ms.");

                // Attempt to set the submission status to approved
                if (processed && unsuccessfulSubmissions.size() == 0) {
                    database.approveSubmission(submissionID);

                    request.getSession().setAttribute("title", "Submission Successful.");
                    request.getSession().setAttribute("message", "The submission has been successfully entered into DSpace.");
                    request.getSession().setAttribute("return_user", null);
                    request.getSession().setAttribute("return_manager", "Back to approve list");
                    response.sendRedirect("/ingest/Success.jsp");
                } else if (processed && unsuccessfulSubmissions.size() != 0) {
                    request.getSession().setAttribute("title", "Complete Submission Unsuccessful.");
                    request.getSession().setAttribute("message", "An error has been encountered by one or more of the " +
                            "individual submissions in the CSV batch. Number of failed individual submissions: " +
                            unsuccessfulSubmissions.size() + " out of a total of " + submissionCount + " in the batch." +
                            " The " + (submissionCount - unsuccessfulSubmissions.size()) + " successful submissions have " +
                            "been entered into DSpace.</br>" +
                            "This submission has not been marked as approved.</br>");
                    request.getSession().setAttribute("return_user", null);
                    request.getSession().setAttribute("return_manager", "Back to approve list");
                    request.getSession().setAttribute("unsuccessful_submissions", getUnsuccessfulSubmissionsString());
                    request.getSession().setAttribute("unsuccessful_submissions_download", writeUnsuccessfulSubmissionsToFile
                            (submission));
                    response.sendRedirect("/ingest/Error.jsp");
                } else {
                    request.getSession().setAttribute("title", "Submission Unsuccessful.");
                    request.getSession().setAttribute("message", "An error occurred when attempting to post the submission " +
                            "to the DSpace REST API. The submission could not be entered into DSpace at this time.");
                    request.getSession().setAttribute("return_user", null);
                    request.getSession().setAttribute("return_manager", "Back to approve list");
                    response.sendRedirect("/ingest/Error.jsp");
                }
            } else {
                request.getSession().setAttribute("title", "Submission Unsuccessful.");
                request.getSession().setAttribute("message", "The submission could not be read from the database. The " +
                        "submission could not be entered into DSpace at this time.");
                request.getSession().setAttribute("return_user", null);
                request.getSession().setAttribute("return_manager", "Back to approve list");
                response.sendRedirect("/ingest/Error.jsp");
            }
        } else {
            request.getSession().setAttribute("title", "Approve Unsuccessful.");
            request.getSession().setAttribute("message", "You do not have permission to approve submissions.");
            request.getSession().setAttribute("return_manager", null);
            request.getSession().setAttribute("return_user", null);
            response.sendRedirect("/ingest/Error.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.sendRedirect("/Login.jsp");
    }

    /*
     * Load the configuration file to connect to the rest api.
     */
    private void loadConnectionConfig() {

        ResourceBundle resourceBundle = ResourceBundle.getBundle("rest");
        hostname = resourceBundle.getString("rest.hostname");
        port = resourceBundle.getString("rest.port");
    }


    private String getUnsuccessfulSubmissionsString() {

        StringBuilder toReturn = new StringBuilder();
        int count = 0;
        int max = 10;

        while (count < unsuccessfulSubmissions.size() && count < max) {
            toReturn.append(unsuccessfulSubmissions.get(count)).append("</br>");
            count++;
        }

        return toReturn.toString();
    }

    /*
     * Saves all the unsuccessful submissions to a file so the user can download them and try again later.
     */
    private String writeUnsuccessfulSubmissionsToFile(Submission submission) {

        String pathToFile = null;

        String filename = "failed_submissions_" + submission.getFilename();

        // Save the file in the upload directory
        String fileUploadPath = "upload";
        String rootServerPath = getServletContext().getRealPath("");
        String uploadPath = rootServerPath + File.separator + fileUploadPath;

        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists()) {
            uploadDir.mkdir();
        }

        try {
            File csvFile = new File(uploadDir, filename);
            if (!csvFile.exists()) {
                csvFile.createNewFile();
            }

            FileWriter fileWriter = new FileWriter(csvFile.getAbsoluteFile());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);

            for (String line : unsuccessfulSubmissions) {
                bufferedWriter.write(line + "\n");
            }

            bufferedWriter.close();

            pathToFile = fileUploadPath + File.separator + csvFile.getName();

        } catch (Exception e) {
            logErr("Error writing failed submissions file. " + e.getMessage());
        }


        return pathToFile;
    }

    /**
     * Iterate through each line of the csv file and post each as a submission to the REST api.
     * <p>
     * Individual unsuccessful submissions are saved in the unsuccessful submissions array.
     * <p>
     * If an individual submission is unsuccessful it will retry once.
     *
     * @param submission The submission object.
     * @return True if the batch was successfully iterated through, false other wise. See unsuccessfulSubmissions for a list of
     * unsuccessful submissions.
     */
    public boolean processCSV(Submission submission) {

        boolean submittedBatch = false;
        boolean submittedSingle = false;

        unsuccessfulSubmissions = new ArrayList<String>();
        submissionCount = 0;
        int subCount = 0;
        int maxSubCount = 50;

        long startTime;
        long endTime;

        // Get the collection ID
        String collectionID = submission.getCollectionID();

        // Get the dcTitles array
        String[] dcTitles = processDcTitles(submission.getDcTitles());

        // Get the csv file separator
        separator = submission.getSeparator().equals("\\t") ? '\t' : submission.getSeparator().charAt(0);

        try {
            // Get the csv file
            InputStream csvFile = new FileInputStream(new File(submission.getMetadataFileAbsolutePath()));
            CSVReader reader = new CSVReader(new InputStreamReader(csvFile), separator);
            String[] nextLine;

            // If it the csv file has a header skip the first line.
            if (hasHeader){
                reader.readNext();
            }

            while ((nextLine = reader.readNext()) != null) {
                submissionCount++;
                subCount++;

                if (subCount == maxSubCount) {
                    subCount = 0;
                    logln("Processed " + submissionCount + " items.");
                }

                startTime = System.currentTimeMillis();
                submittedSingle = postSingleSubmission(getJSON(dcTitles, nextLine), collectionID);
                endTime = System.currentTimeMillis();

                if (!submittedSingle) {
                    errorTime += (endTime - startTime);

                    logErr("Retrying submission...");
                    startTime = System.currentTimeMillis();
                    submittedSingle = postSingleSubmission(getJSON(dcTitles, nextLine), collectionID);
                    endTime = System.currentTimeMillis();

                    if (!submittedSingle) {
                        errorTime += (endTime - startTime);
                        unsuccessfulSubmissions.add(arrayToString(nextLine));
                        logErr("Retry failed.");
                    } else {
                        logln("Retry successful.");
                    }
                }
            }

            submittedBatch = true;
        } catch (Exception e) {
            logErr("An error occurred when parsing the CSV file. " + e.getMessage());
        }

        return submittedBatch;
    }

    public String arrayToString(String[] array) {

        StringBuilder arrayString = new StringBuilder();

        for (String entry : array) {
            arrayString.append(entry).append(separator);
        }

        return arrayString.toString();
    }

    /**
     * Builds a JSON string with the DC titles as keys and csv line as values
     *
     * @param dcTitles Dublin Core keys
     * @param csvLine  One line from the CSV file
     * @return JSON formatted string
     */
    public String getJSON(String[] dcTitles, String[] csvLine) {

        String toReturn = "";

        if (dcTitles.length != csvLine.length) {
            logErr("dcTitles length not the same as csvLine! Continuing anyway...");
        }

        if (dcTitles.length >= csvLine.length) {
            JSONObject jsonObjectMain = new JSONObject();
            JSONObject jsonObject;
            JSONArray jsonArray = new JSONArray();

            for (int i = 0; i < dcTitles.length; i++) {
                jsonObject = new JSONObject();
                jsonObject.put("language", "en_US");
                jsonObject.put("value", csvLine[i]);
                jsonObject.put("key", dcTitles[i]);

                jsonArray.add(jsonObject);
            }

            jsonObjectMain.put("metadata", jsonArray);

            toReturn = jsonObjectMain.toString();
        }

        return toReturn;
    }

    /**
     * Converts an array save as a string back to an array.
     *
     * @param dcTitles The dublin core keys
     * @return String array of the dublin core values
     */
    private String[] processDcTitles(String dcTitles) {
        // Remove the first char [
        dcTitles = dcTitles.substring(1);

        // Remove the last char ]
        dcTitles = dcTitles.substring(0, dcTitles.length() - 1);

        // Split array on the , char
        return dcTitles.split(", ");
    }

    /*
     * Posts a single submission to the DSpace REST api. Returns true if the submission was successful.
     * <p>
     * REST api usage: curl -X POST -H "Content-Type: application/json" -H "rest-dspace-token: <token>"
     *  -H "accept: application/json" --data "<escaped-json-metadata>"
     *  http://localhost:8080/rest/collections/<collection-id>/items
     *
     * @param metadata String of the metadata to post to DSpace.
     * @return True if submission was successful, false otherwise.
     */
    private boolean postSingleSubmission(String metadata, String collectionID) {

        boolean submission = false;

        try {

            // Post the metadata to the REST api endpoint
            URL url = new URL("http://" + hostname + ":" + port + "/rest/collections/" + collectionID + "/items");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("rest-dspace-token", token);
            conn.setRequestProperty("accept", "application/json");

            OutputStream os = conn.getOutputStream();
            os.write(metadata.getBytes());
            os.flush();

            // A successful submit will return a HTTP response code of 200
            if (conn.getResponseCode() != 200) {
                submission = false;
                logErr("Post Submission Failed. HTTP error code: " + conn.getResponseCode());
                logErr("Failed URL: " + url.toString());
            } else {
                submission = true;
                conn.disconnect();
            }

        } catch (IOException e) {
            logErr("An error occurred when posting to the REST api. " + e.getMessage());
        }

        return submission;
    }

    private void logln(String str) {

        System.out.println("Approve Submission: " + str);
    }

    private void logErr(String str) {

        System.err.println("Approve Submission: " + str);
    }

}
