package za.ac.uct.ingestion.services;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Darryl Meyer
 * 23 September 2015
 * <p>
 * Test case for fetching the collections and communities form the REST API.
 */
public class CollectionManagerTest {

    @Test
    public void testGetAllCollectionsXMLString() throws Exception {
        String restResponse = CollectionManager.getAllCollectionsString();
        boolean validHTML = restResponse.startsWith("<option");
        if (!validHTML) {
            System.err.println("The string constructed does not match the expected formatting.");
        }

        assertEquals(true, validHTML);
    }
}