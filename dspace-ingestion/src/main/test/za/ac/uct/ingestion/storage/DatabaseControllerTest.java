package za.ac.uct.ingestion.storage;


import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Darryl Meyer
 * 23 September 2015
 * <p>
 * Test cases for the Ingestion Manager database.
 * <p>
 * This test should be run while the Ingestion Manager is running.
 */
public class DatabaseControllerTest {
    DatabaseController databaseController;

    @Before
    public void before() {

        String path = System.getProperty("user.dir") + File.separator + "target" +
                File.separator + "dspace-ingestion-1.0" + File.separator + "WEB-INF" +
                File.separator + "classes" + File.separator;
        databaseController = new DatabaseController(path);
    }

    @Test
    public void testOpenDatabase() throws Exception {

        boolean opened = databaseController.openDatabase();

        assertTrue(opened);
    }

    @Test
    public void testInsertUserRoleTable() throws Exception {

        boolean tableCreated = databaseController.createUserRoleTable();

        assertTrue(tableCreated);
    }

    @Test
    public void testTableExists() throws Exception {

        boolean tableExists = databaseController.tableExists("im_user_role");

        assertTrue(tableExists);
    }

    @Test
    public void testCheckRole() throws Exception {
        String userRole = databaseController.checkUserRole("myrdar003@myuct.ac.za");

        assertEquals("manager", userRole);
    }

    @Test
    public void testCloseDatabase() throws Exception {
        boolean databaseClosed = databaseController.closeDatabase();

        assertTrue(databaseClosed);
    }
}